<?php
function full_title($title=null) 
{
     $app_name = config('app.name', 'PicGallery');
    if ($title != null) {
        return $title . ' - ' . $app_name;
    }
    return $app_name;
}
?>