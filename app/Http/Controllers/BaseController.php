<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Picture;

class BaseController extends Controller
{
    public function home() 
    {
        $pictures = Picture::with('user')
                            ->orderBy('created_at', 'desc')
                            ->paginate(20);
        return view('base.home', ['pictures' => $pictures]);
    }

    public function mypage() 
    {
        $user = Auth::user();
        $pictures = Picture::orderBy('created_at', 'desc')
                            ->where('user_id', $user->id)
                            ->paginate(10);
        return view('base.mypage', ['user' => $user, 'pictures' => $pictures]);
    }
}
