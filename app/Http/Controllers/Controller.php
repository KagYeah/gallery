<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function blankToSpace($str) {
        $s = "";
        foreach(preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY) as $c) {
            if ($c == '　' || $c == '\t') {
                $s .= ' ';
            } else {
                $s .= $c;
            }
        }
        return $s;
    }
}
