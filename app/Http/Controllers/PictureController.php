<?php

namespace App\Http\Controllers;

use App\Picture;
use App\Favorite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class PictureController extends Controller
{
    public function index()
    {
        $pictures = Picture::with('user')
                            ->orderBy('created_at', 'desc')
                            ->paginate(30);
        return view('picture.index', ['pictures' => $pictures]);
    }

    public function create()
    {
        $user = Auth::user();
        return view('picture.create', ['user' => $user]);
    }

    public function store(Request $request)
    {
        $this->validate($request, Picture::$rules);

        $pictureFile = $request->file('picture');
        $extension = $pictureFile->getClientOriginalExtension();

        do {
            $file_name = Str::random(20).'.'.$extension;
        } while (Picture::where('path', $file_name)->first() != null);

        $this->savePicture($pictureFile, $file_name);

        $form = $request->all();
        $form['path'] = $file_name;
        unset($form['_token']);
        unset($form['_method']);
        unset($form['picture']);
        $picture = Picture::create($form);
        return redirect(route('picture.show', ['picture' => $picture->id]));
    }

    public function show($id)
    {
        $picture = Picture::find($id);
        $user = Auth::user();

        if ($user == null) {
            $favorite = null;
        } else {
            $favorite = Favorite::where('user_id', $user->id)
                                ->where('picture_id', $picture->id)
                                ->first();
        }

        return view('picture.show', [
            'picture' => $picture, 
            'user' => $user, 
            'favorite' => $favorite
        ]);
    }

    public function edit($id)
    {
        $this->checkUser($id);
        
        $picture = Picture::find($id);
        return view('picture.edit', ['picture' => $picture]);
    }

    public function update($id, Request $request)
    {
        $this->checkUser($id);

        $this->validate($request, [
            'title' => ['required', 'string', 'max:20'],
            'comment' => ['max:100'],
        ]);

        $form = $request->all();
        unset($form['_token']);
        unset($form['_method']);
        unset($form['picture']);
        $picture = Picture::find($id);
        $picture->fill($form)->save();

        return redirect(route('picture.show', ['picture' => $picture->id]));
    }

    public function destroy($id)
    {
        $this->checkUser($id);

        $picture = Picture::find($id);

        if (Storage::exists('public/pictures/'.$picture->path)) {
            Storage::delete('public/pictures/'.$picture->path);
        }
        $picture->delete();

        return redirect('/mypage');
    }

    public function download($id) 
    {
        $picture = Picture::find($id);
        return Storage::download('public/pictures/'.$picture->path);
    }

    public function search(Request $request) 
    {
        $input = $request->input;
        if ($input == null) {
            $title = '画像を検索';
            $pictures = [];
            $input = '';
            $order = '';
        } else {
            $input = $this->blankToSpace($request->input);
            $title = '「'.$input.'」の結果';
            
            $order = $request->order;
            $pictures = Picture::with('user');
            switch ($order) {
                case '新しい順':
                    $pictures = $pictures->orderBy('updated_at', 'desc');
                    break;
                case '古い順':
                    $pictures = $pictures->orderBy('updated_at', 'asc');
                    break;
                case 'いいね順':
                    $pictures = $pictures->orderBy('favorite', 'desc');
                    break;
                default:
                    $pictures = $pictures->orderBy('updated_at', 'desc');
            }
            
            if ($input != null)  {
                $keys = explode(' ', $input);
                foreach ($keys as $key) {
                    $pictures = $pictures->where('title', 'like', '%'.$key.'%');
                }
            }
            $pictures = $pictures->paginate(30);
        }

        return view('picture.search', [
            'title' => $title, 
            'pictures' => $pictures, 
            'input' => $input, 
            'order' => $order
        ]);
    }

    private function checkUser($picture_id) 
    {
        $user = Auth::user();
        if ($user == null || $user->id != Picture::find($picture_id)->user_id) {
            abort(403);
        }
    }

    private function savePicture($image, $file_name) 
    {
        // get instance
        $img = \Image::make($image);
        // resize
        $img->fit(512, null, function($constraint){
            $constraint->upsize(); 
        });
        // save
        $save_path = 'public/pictures/'.$file_name;
        Storage::put($save_path, (string) $img->encode());
    }
}
