<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Picture;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function show($id) 
    {
        $user = User::find($id);
        $pictures = Picture::orderBy('created_at', 'desc')
                            ->where('user_id', $user->id)
                            ->paginate(10);
        return view('user.show', ['user' => $user, 'pictures' => $pictures]);
    }

    public function edit($id) 
    {
        $this->checkUser($id);
        
        $user = Auth::user();
        return view('user.edit', ['user' => $user]);
    }

    public function update($id, UserRequest $request) 
    {
        $this->checkUser($id);

        $profileImage = $request->file('profile_image');
        
        $form = $request->all();
        if ($profileImage != null) {
            $form['profile_image'] = $this->saveProfileImage($profileImage, $id); // return file name
        }
        unset($form['_token']);
        unset($form['_method']);
        $user = Auth::user();
        $user->fill($form)->save();
        return redirect('/mypage');
    }

    public function del() 
    {
        $user = Auth::user();
        return view('user.del', ['user' => $user]);
    } 

    public function destroy($id) 
    {
        $this->checkUser($id);
        
        $user = Auth::user();
        if ($user->profile_image != 'default.png' && Storage::exists('public/profiles/'.$user->profile_image)) {
            Storage::delete('public/profiles/'.$user->profile_image);
        }
        $user->delete();

        return redirect('/home');
    }
    
    public function search(Request $request) 
    {
        $input = $request->input;
        if ($input == null) {
            $title = 'ユーザーを検索';
            $users = [];
            $input = '';
        } else {
            $input = $this->blankToSpace($input);
            $title = '「'.$input.'」の結果';

            $users = User::orderBy('created_at', 'desc');
            if ($input != null)  {
                $keys = explode(' ', $input);
                foreach ($keys as $key) {
                    $users = $users->where('name', 'like', '%'.$key.'%');
                }
            }
            $users = $users->paginate(30);
        }

        return view('user.search', [
            'title' => $title, 
            'users' => $users, 
            'input' => $input
        ]);
    }

    private function checkUser($user_id) 
    {
        $user = Auth::user();
        if ($user == null || $user->id != $user_id) {
            abort(403);
        }
    }

    private function saveProfileImage($image, $id) 
    {
        // get instance
        $img = \Image::make($image);
        // resize
        $img->fit(100, 100, function($constraint){
            $constraint->upsize(); 
        });
        // save
        $file_name = 'profile_'.$id.'.'.$image->getClientOriginalExtension();
        $save_path = 'public/profiles/'.$file_name;
        Storage::put($save_path, (string) $img->encode());

        return $file_name;
    }
}
