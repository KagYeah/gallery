<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Picture;
use App\Favorite;

class FavoriteController extends Controller
{
    public function store(Request $request) 
    {
        $user_id = $request->user_id;
        $this->checkUser($user_id);

        $picture_id = $request->picture_id;
        $favorite = Favorite::where('user_id', $user_id)
                            ->where('picture_id', $picture_id)
                            ->first();
                            
        if (User::find($user_id) && ($picture = Picture::find($picture_id)) && $favorite == null) {
            $picture->favorite++;
            $picture->save();
            Favorite::create([
                'user_id' => $user_id,
                'picture_id' => $picture_id,
            ]);
        }

        return redirect(route('picture.show', ['picture' => $picture->id]));
    }

    public function destroy($id) 
    {
        $favorite = Favorite::find($id);
        if ($favorite != null) {
            $this->checkUser($favorite->user_id);

            $picture = Picture::find($favorite->picture_id);
            $picture->favorite--;
            $picture->save();
            $favorite->delete();

            return redirect(route('picture.show', ['picture' => $favorite->picture_id]));
        }

        return redirect(route('home'));
    }

    private function checkUser($user_id) 
    {
        $user = Auth::user();
        if ($user == null || $user->id != $user_id) {
            abort(403);
        }
    }
}
