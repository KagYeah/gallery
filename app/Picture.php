<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    // protected $table = 'pictures';

    protected $fillable = [
        'user_id', 'title', 'path', 'comment',
    ];

    public static $rules = [
        'title' => ['required', 'string', 'max:20'],
        'picture' => ['required', 'image', 'max:2048'],
        'comment' => ['max:100'],
    ];

    public function user() 
    {
        return $this->belongsTo('App\User');
    }

    public function favorites() 
    {
        return $this->hasMany('App\Favorite');
    }

    protected static function boot() 
    {
        parent::boot();
        static::deleting(function($model) {
            foreach($model->favorites as $favorite) {
                $favorite->delete();
            }
        });
    }
}
