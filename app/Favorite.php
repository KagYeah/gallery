<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $fillable = [
        'user_id',
        'picture_id',
    ];

    public function user() 
    {
        return $this->belongsTo('App\User');
    }

    public function picture() 
    {
        return $this->belongsTo('App\Picture');
    }
}
