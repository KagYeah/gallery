<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/', 'BaseController@home');
Route::get('/home', 'BaseController@home')->name('home');
Route::view('/search', 'base.search')->name('search');

Route::get('/user/search', 'UserController@search')->name('user.search');
Route::post('/user/search', 'UserController@search');

Route::get('/picture/search', 'PictureController@search')->name('picture.search');
Route::post('/picture/search', 'PictureController@search');

Route::get('download/{id}', 'PictureController@download')->name('download');

Route::group(['middleware' => ['auth', 'verified']], function () {
    Route::view('/verified', 'auth.verified');
    Route::resource('picture', 'PictureController')->except('index', 'show');
    Route::resource('favorite', 'FavoriteController')->only('store', 'destroy');
});

Route::resource('picture', 'PictureController')->only('index', 'show');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/user/delete', 'UserController@del')->name('user.delete');
    Route::resource('user', 'UserController')->except('show', 'create', 'store');
    Route::get('/mypage', 'BaseController@mypage')->name('mypage');
});

Route::resource('user', 'UserController')->only('show');