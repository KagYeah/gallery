<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Picture;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

class PictureTest extends TestCase
{
    use DatabaseTransactions;

    public function testIndex() {
        $response = $this->get(route('picture.index'));
        $response->assertOk();
    }

    public function testShow() {
        $picture = factory(Picture::class)->create();
        $response = $this->get(route('picture.show', ['picture'=>$picture->id]));
        $response->assertOk();
    }

    public function testCreate() {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)
                        ->get(route('picture.create'));
        $response->assertOk();
    }

    public function testGuestCreate() {
        $response = $this->get(route('picture.create'));
        $response->assertRedirect(route('login'));
    }

    public function testStore() {
        $user = factory(User::class)->create();
        $count = Picture::count();
        Storage::fake('local');

        $response = $this->actingAs($user)
                        ->post(route('picture.store'), [
                            'user_id' => $user->id,
                            'picture' => UploadedFile::fake()->image('test.png'),
                            'title' => 'test',
                            'comment' => 'test comment',
                        ]);
        $picture = Picture::orderBy('created_at', 'desc')->first();

        Storage::disk('local')->assertExists('public/pictures/'.$picture->path);

        $this->assertEquals(Picture::count(), $count+1);
        $response->assertRedirect(route('picture.show', ['picture' => $picture->id]));
    }

    public function testGuestStore() {
        $user = factory(User::class)->create();
        $count = Picture::count();
        Storage::fake('local');

        $response = $this->post(route('picture.store'), [
                            'user_id' => $user->id,
                            'picture' => UploadedFile::fake()->image('test.png'),
                            'title' => 'test',
                            'comment' => 'test comment',
                        ]);

        $response->assertRedirect(route('login'));
        $this->assertEquals($count, Picture::count());
    }

    public function testEdit() {
        $user = factory(User::class)->create();
        $picture = factory(Picture::class)->state('no user_id')->create(['user_id' => $user->id]);

        $response = $this->actingAs($user)
                        ->get(route('picture.edit', ['picture' => $picture->id]));

        $response->assertOk();
    }

    public function testEditOtherUsersPicture() {
        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $picture = factory(Picture::class)->state('no user_id')->create(['user_id' => $user2->id]);

        $response = $this->actingAs($user1)
                        ->get(route('picture.edit', ['picture' => $picture->id]));

        $response->assertForbidden();
    }

    public function testUpdate() {
        $user = factory(User::class)->create();
        $picture = factory(Picture::class)->state('no user_id')
                                        ->create([
                                            'user_id' => $user->id,
                                            'title' => 'test',
                                            'comment' => 'test comment',
                                        ]);
        $count = Picture::count();

        $response = $this->actingAs($user)
                        ->patch(route('picture.update', ['picture' => $picture->id]), [
                            'title' => 'updated',
                            'comment' => 'updated comment',
                        ]);
        
        $response->assertRedirect(route('picture.show', ['picture' => $picture->id]));
        
        $picture = Picture::find($picture->id);
        $this->assertEquals('updated', $picture->title);
        $this->assertEquals('updated comment', $picture->comment);

        $this->assertEquals($count, Picture::count());
    }

    public function testUpdateOtherUsersPicture() {
        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $picture = factory(Picture::class)->state('no user_id')
                                        ->create([
                                            'user_id' => $user2->id,
                                            'title' => 'test',
                                            'comment' => 'test comment',
                                        ]);
        $count = Picture::count();

        $response = $this->actingAs($user1)
                        ->patch(route('picture.update', ['picture' => $picture->id]), [
                            'title' => 'updated',
                            'comment' => 'updated comment',
                        ]);
        
        $response->assertForbidden();
        
        $picture = Picture::find($picture->id);
        $this->assertEquals('test', $picture->title);
        $this->assertEquals('test comment', $picture->comment);

        $this->assertEquals($count, Picture::count());
    }

    public function testDestroy() {
        $user = factory(User::class)->create();
        $picture = factory(Picture::class)->state('no user_id')
                                        ->create([
                                            'user_id' => $user->id,
                                        ]);
        $count = Picture::count();

        $response = $this->actingAs($user)
                        ->delete(route('picture.destroy', ['picture' => $picture->id]));
        
        $response->assertRedirect(route('mypage'));

        $this->assertEquals($count-1, Picture::count());
    }

    public function testDestroyOtherUsersPicture() {
        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $picture = factory(Picture::class)->state('no user_id')
                                        ->create([
                                            'user_id' => $user2->id,
                                        ]);
        $count = Picture::count();

        $response = $this->actingAs($user1)
                        ->delete(route('picture.destroy', ['picture' => $picture->id]));
        
        $response->assertForbidden();

        $this->assertEquals($count, Picture::count());
    }

    public function testDownload() {
        $user = factory(User::class)->create();
        Storage::fake('local');

        $response = $this->actingAs($user)
                        ->post(route('picture.store'), [
                            'user_id' => $user->id,
                            'picture' => UploadedFile::fake()->image('test.png'),
                            'title' => 'test',
                            'comment' => 'test comment',
                        ]);
        $picture = Picture::orderBy('created_at', 'desc')->first();

        $response = $this->get(route('download', ['id' => $picture->id]));

        $response->assertOk();
    }

    public function testSearch() {
        $response = $this->get(route('picture.search'));
        $response->assertOk();
    }
}
