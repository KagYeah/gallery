<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Auth;
use App\User;

class BaseTest extends TestCase
{
    use DatabaseTransactions;

    public function testHome() 
    {
        $response = $this->get(route('home'));
        $response->assertOk();
    }

    public function testMypage() 
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)
                        ->get(route('mypage'));

        $response->assertOk();
    }

    public function testGuestVisitsToMypage() 
    {
        $response = $this->get(route('mypage'));

        $response->assertRedirect(route('login'));
    }

    public function testNoRoute() 
    {
        $response = $this->get('no_route');
        $response->assertNotFound();
    }
}
