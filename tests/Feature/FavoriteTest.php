<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Picture;
use App\Favorite;

class FavoriteTest extends TestCase
{
    use DatabaseTransactions;

    public function testStore() 
    {
        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $picture = factory(Picture::class)->state('no user_id')
                                        ->create(['user_id' => $user2->id]);
        $count = Favorite::count();

        $response = $this->actingAs($user1)
                        ->post(route('favorite.store'), [
                            'user_id' => $user1->id,
                            'picture_id' => $picture->id,
                        ]);

        $response->assertRedirect(route('picture.show', ['picture' => $picture->id]));

        $this->assertEquals($count+1, Favorite::count());
    }

    public function testStoreAsOtherUser() 
    {
        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $picture = factory(Picture::class)->state('no user_id')
                                        ->create(['user_id' => $user2->id]);
        $count = Favorite::count();

        $response = $this->actingAs($user2)
                        ->post(route('favorite.store'), [
                            'user_id' => $user1->id,
                            'picture_id' => $picture->id,
                        ]);

        $response->assertForbidden();

        $this->assertEquals($count, Favorite::count());
    }

    public function testGuestStore() 
    {
        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $picture = factory(Picture::class)->state('no user_id')
                                        ->create(['user_id' => $user2->id]);
        $count = Favorite::count();

        $response = $this->post(route('favorite.store'), [
            'user_id' => $user1->id,
            'picture_id' => $picture->id,
        ]);

        $response->assertRedirect(route('login'));

        $this->assertEquals($count, Favorite::count());
    }

    public function testStoreDouble() 
    {
        $favorite = factory(Favorite::class)->create();

        $count = Favorite::count();

        $response = $this->actingAs(User::find($favorite->user_id))
                        ->post(route('favorite.store'), [
                            'user_id' => $favorite->user_id,
                            'picture_id' => $favorite->picture_id,
                        ]);

        $response->assertRedirect(route('picture.show', ['picture' => $favorite->picture_id]));

        $this->assertEquals($count, Favorite::count());
    }

    public function testDestroy() 
    {
        $favorite = factory(Favorite::class)->create();

        $count = Favorite::count();

        $response = $this->actingAs(User::find($favorite->user_id))
                        ->delete(route('favorite.destroy', ['favorite' => $favorite->id]));

        $response->assertRedirect(route('picture.show', ['picture' => $favorite->picture_id]));

        $this->assertEquals($count-1, Favorite::count());
    }

    public function testDestroyAsOtherUser() 
    {
        $user = factory(User::class)->create();
        $favorite = factory(Favorite::class)->create();

        $count = Favorite::count();

        $response = $this->actingAs($user)
                        ->delete(route('favorite.destroy', ['favorite' => $favorite->id]));

        $response->assertForbidden();

        $this->assertEquals($count, Favorite::count());
    }

    public function testGuestDestroy() 
    {
        $favorite = factory(Favorite::class)->create();

        $count = Favorite::count();

        $response = $this->delete(route('favorite.destroy', ['favorite' => $favorite->id]));

        $response->assertRedirect(route('login'));

        $this->assertEquals($count, Favorite::count());
    }

    public function testDestroyDouble() 
    {
        $favorite = factory(Favorite::class)->create();

        $count = Favorite::count();

        $response = $this->actingAs(User::find($favorite->user_id))
                        ->delete(route('favorite.destroy', ['favorite' => $favorite->id]));
        
        $this->assertEquals($count-1, Favorite::count());

        $response = $this->actingAs(User::find($favorite->user_id))
                        ->delete(route('favorite.destroy', ['favorite' => $favorite->id]));

        $response->assertRedirect(route('home'));

        $this->assertEquals($count-1, Favorite::count());
    }
}
