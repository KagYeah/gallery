<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    public function testShow() 
    {
        $user = factory(User::class)->create();
        $response = $this->get(route('user.show', ['user' => $user->id]));
        $response->assertOk();
    }

    public function testEdit() 
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)
                        ->get(route('user.edit', ['user' => $user->id]));
        $response->assertOk();
    }

    public function testEditOtherUser() 
    {
        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();

        $response = $this->actingAs($user1)
                        ->get(route('user.edit', ['user' => $user2->id]));
        $response->assertForbidden();
    }

    public function testGuestEdit() 
    {
        $user = factory(User::class)->create();

        $response = $this->get(route('user.edit', ['user' => $user->id]));
        $response->assertRedirect(route('login'));
    }

    public function testUpdate() 
    {
        $user = factory(User::class)->create([
            'name' => 'user',
            'email' => 'user@example.com',
        ]);

        $count = User::count();

        $response = $this->actingAs($user)
                        ->patch(route('user.update', ['user' => $user->id]), [
                            'name' => 'updated',
                            'email' => 'updated@gmail.com',
                            'body' => 'updated comment',
                        ]);
        $response->assertRedirect(route('mypage'));

        $this->assertEquals('updated', $user->name);
        $this->assertEquals('updated@gmail.com', $user->email);
        $this->assertEquals('updated comment', $user->body);

        $this->assertEquals($count, User::count());
    }

    public function testUpdateProfileImage() 
    {
        $user = factory(User::class)->create();
        Storage::fake('local');

        $response = $this->actingAs($user)
                        ->patch(route('user.update', ['user' => $user->id]), [
                            'name' => 'updated',
                            'email' => 'updated@gmail.com',
                            'body' => 'updated comment',
                            'profile_image' => UploadedFile::fake()->image('image.jpg')
                        ]);
        Storage::disk('local')->assertExists('public/profiles/'.$user->profile_image);
    }

    public function testUpdateOtherUser() 
    {
        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();

        $response = $this->actingAs($user1)
                        ->patch(route('user.update', ['user' => $user2->id]), [
                            'name' => 'updated',
                            'email' => 'updated@gmail.com',
                            'body' => 'updated comment',
                        ]);
        $response->assertForbidden();
    }

    public function testGuestUpdate() 
    {
        $user = factory(User::class)->create();

        $response = $this->patch(route('user.update', ['user' => $user->id]), [
            'name' => 'updated',
            'email' => 'updated@gmail.com',
            'body' => 'updated comment',
        ]);
        $response->assertRedirect(route('login'));
    }

    public function testDel() 
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)
                        ->get(route('user.delete'));
        $response->assertOk();
    }

    public function testDeleteOtherUser() 
    {
        $user = factory(User::class)->create();
        $response = $this->get(route('user.delete'));
        $response->assertRedirect(route('login'));
    }

    public function testDestroy() 
    {
        $user = factory(User::class)->create();
        $count = User::count();

        $response = $this->actingAs($user)
                        ->delete(route('user.destroy', ['user' => $user->id]));

        $response->assertRedirect(route('home'));

        $this->assertEquals($count-1, User::count());
    }

    public function testDestroyOtherUser() 
    {
        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $count = User::count();

        $response = $this->actingAs($user1)
                        ->delete(route('user.destroy', ['user' => $user2->id]));

        $response->assertForbidden();

        $this->assertEquals($count, User::count());
    }

    public function testSearch() 
    {
        $response = $this->get(route('user.search'));
        $response->assertOk();
    }
}
