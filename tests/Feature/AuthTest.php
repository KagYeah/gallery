<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\User;

class AuthTest extends TestCase
{
    use DatabaseTransactions;

    public function testLogin() 
    {
        $user = factory(User::class)->create([
            'password' => Hash::make('testpass')
        ]);

        $this->assertFalse(Auth::check());

        $response = $this->post(route('login'),[
            'email' => $user->email,
            'password' => 'testpass',
        ]);

        $this->assertTrue(Auth::check());

        $response->assertRedirect(route('mypage'));
    }

    public function testInvalidLogin() 
    {
        $user = factory(User::class)->create([
            'password' => Hash::make('testpass')
        ]);

        $this->assertFalse(Auth::check());

        $response = $this->post(route('login'),[
            'email' => $user->email,
            'password' => 'invalid',
        ]);

        $this->assertFalse(Auth::check());

        $response->assertSessionHasErrors(['email']);
        
        $this->assertEquals('認証情報と一致するレコードがありません。',
        session('errors')->first('email'));
    }

    public function testRegister() 
    {
        $count = User::count();

        $response = $this->post(route('register'),[
            'name' => 'testuser',
            'email' => 'test@example.com',
            'password' => "testpass",
            'password_confirmation' => 'testpass',
        ]);

        $this->assertTrue(Auth::check());
        
        $this->assertEquals(User::count(), $count+1);

        $response->assertRedirect(route('mypage'));
    }

    public function testInvalidResigter() 
    {
        $count = User::count();

        $response = $this->post(route('register'),[
            'name' => '',
            'email' => 'test',
            'password' => "testpass",
            'password_confirmation' => 'foobar',
        ]);

        $response->assertSessionHasErrors(['name', 'email', 'password']);

        $this->assertEquals('名前は、必ず指定してください。',
        session('errors')->first('name'));
        $this->assertEquals('メールアドレスは、有効なメールアドレス形式で指定してください。',
        session('errors')->first('email'));
        $this->assertEquals('パスワードとパスワード確認が一致しません。',
        session('errors')->first('password'));

        $this->assertEquals(User::count(), $count);
    }
}
