<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Picture;
use App\Favorite;

class PictureModelTest extends TestCase
{
    use DatabaseTransactions;

    public function testDeleting() 
    {
        $picture = factory(Picture::class)->create();
        $favoriteCount = Favorite::count();
        $picture->favorites()->createMany(
            factory(Favorite::class, 3)->state('no picture_id')->make()->toArray()
        );

        $this->assertEquals($favoriteCount+3, Favorite::count());

        $picture->delete();

        $this->assertEquals($favoriteCount, Favorite::count());
    }
}
