<?php

namespace Tests\Unit;

use Tests\TestCase;

class HelperTest extends TestCase
{
    public function testFullTitle() 
    {
        $this->assertEquals('Home - PicGallery', full_title('Home'));
        $this->assertEquals('PicGallery', full_title());
    }
}
