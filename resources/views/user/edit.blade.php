@extends('layouts.layout')

@section('title', full_title('プロフィール編集'))
    
@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header">プロフィール編集</div>
          <div class="card-body">
            <form method="post" action="{{ route('user.update', ['user' => $user->id]) }}" enctype="multipart/form-data">
              @csrf
              @method('PATCH')

              <div class="form-group row">
                <label for="profile_image" class="col-md-4 col-form-label text-md-right">プロフィール画像</label>

                <div class="col-md-6 align-self-center">
                  <label for="profile_image" class="btn"><img src="{{ asset('storage/profiles/'.$user->profile_image) }}" id="img">
                    <input id="profile_image" type="file" class="@error('profile_image') is-invalid @enderror" name="profile_image" onchange="previewImage(this);">
                  </label>

                  @error('profile_image')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">名前</label>

                <div class="col-md-6">
                  <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="@if(old('name') != null){{ old('name') }}@else{{ $user->name }}@endif" required autocomplete="name" autofocus>

                  @error('name')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">メールアドレス</label>

                <div class="col-md-6">
                  <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="@if (old('email') != null) {{ old('email') }} @else {{ $user->email }} @endif" required autocomplete="email">

                  @error('email')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label for="body" class="col-md-6 offset-md-3 col-form-label text-md-center">自己紹介文</label>
              </div>

              <div class="form-group row">
                <div class="col-md-10 offset-md-1">

                  <textarea id="body" class="form-control @error('body') is-invalid @enderror" name="body">@if (old('body') != null){{ old('body') }}@else{{ $user->body }}@endif</textarea>
                  @error('body')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <div class="col-md-6 offset-md-3 text-center">
                  <button type="submit" class="btn btn-primary">
                    変更
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script>
    function previewImage(obj)
    {
      var fileReader = new FileReader();
      fileReader.onload = (function() {
        document.getElementById('img').src = fileReader.result;
      });
      fileReader.readAsDataURL(obj.files[0]);
    }
  </script>
@endsection