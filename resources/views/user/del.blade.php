@extends('layouts.layout')

@section('title', full_title('退会'))

@section('content')
<div class="profile container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card destroy-user">
        <div class="card-header">
          退会
        </div>
        <div class="card-body">
          <p>退会する場合、以下の事項に注意してください。</p>
          <ul>
            <li>退会後でも投稿した写真・画像は残ります。</li>
            <li>削除されたアカウントは復元できません。</li>
          </ul>
          <p>以上を了解した上で下のボタンをクリックしてください。</p>
          <form method="post" action="{{ route('user.destroy', ['user'=>$user->id]) }}">
            @csrf
            @method('DELETE')
            <button class="btn btn-primary" onclick="return confirm('本当に退会しますか？')" >退会する</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection