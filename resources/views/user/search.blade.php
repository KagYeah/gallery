@extends('layouts.layout')

@section('title', full_title($title))


@section('content')
  <div class="search container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <h1>ユーザーを検索</h1>
        <form method="get" action="{{ route('user.search') }}">
          @csrf

          <div class="row m-1 form-group">
            <div class="col-9 p-0">
              <input type="text" name="input" value="{{ $input }}" class="form-control">
            </div>

            <div class="col-3 p-0 text-center">
              <button class="btn btn-primary">検索</button>
            </div>
          </div>
        </form>

        @if ($title != 'ユーザーを検索')
        <h1>{{ $title }}</h1>
        
        <div class="users">
          {{ $users->appends(['input'=>$input])->onEachSide(2)->links() }}

          <table class="table">
            <thead>
              <tr><th>ユーザー名</th></tr>
            </thead>
            <tbody>
              @foreach ($users as $user)
                <tr>
                  <td>
                    <img src="{{ asset('storage/profiles/'.$user->profile_image) }}">　<a href="{{ route('user.show', ['user' => $user->id]) }}">{{ $user->name }}</a>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
          
          {{ $users->appends(['input'=>$input])->onEachSide(2)->links() }}
        </div>
        @else
        キーワードを入力してください
        @endif

      </div>
    </div>
  </div>
@endsection