@extends('layouts.layout')

@section('title', full_title('検索'))

@section('content')
  <div class="search container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="row justify-content-center"><a href="{{ route('picture.search') }}">画像を検索</a></div>
        <div class="row justify-content-center"><a href="{{ route('user.search') }}">ユーザーを検索</a></div>
      </div>
    </div>
  </div>
@endsection