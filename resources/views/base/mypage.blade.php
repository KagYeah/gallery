@extends('layouts.layout')

@section('title', full_title('マイページ'))

@section('content')
  <div class="profile container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header">
            プロフィール
          </div>
          <div class="card-body">
            <img src="{{ asset('storage/profiles/'.$user->profile_image) }}" alt="プロフィール画像">
            <div class="name">
              {{ $user->name }}
            </div>

            @isset ($user->body)
            <div class="body">
              {!! nl2br($user->body) !!}
            </div>
            @endisset

            <a href="{{ route('user.edit', ['user' => $user->id]) }}">プロフィールを編集する</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <section>
    <h1>退会</h1>
    <p>退会する方は<a href="{{ route('user.delete') }}">こちら</a>から</p>
  </section>

  <section class="myposts">
    <h1>投稿した写真・画像</h1>

    {{ $pictures->onEachSide(2)->links() }}

    @foreach ($pictures as $picture)
      <div class="post">
        <h2>{{ $picture->title }}</h2>

        <a href="{{ route('picture.show', ['picture' => $picture->id]) }}"><img src="{{ asset('storage/pictures/'.$picture->path) }}" alt="投稿画像"></a>

        <div class="favorite">
          いいね:{{ $picture->favorite }}
        </div>
      </div>
    @endforeach

    {{ $pictures->onEachSide(2)->links() }}

  </section>
@endsection