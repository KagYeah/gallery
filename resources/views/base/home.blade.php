@extends('layouts.layout')

@section('title', full_title('Home'))

@section('content')
    <section>
        <h1>PicGalleryとは</h1>
        <p>PicGalleryは、写真や画像を投稿または提供するサイトです。</p>
        <p>主な用途は、個人の作品の公開や自由素材の提供を想定しています。</p>
    </section>

    <section>
        <h1>注意事項</h1>
        <p>アップロードする際は、個人を特定できるような情報が映し出されていないか、各個人で確認してください。</p>
        <p>また、アップロードされた作品は他人に使用され得るということを十分理解してください。</p>
    </section>

    @auth
    @else
    <section>
        <h1>新規登録</h1>
        <p>アカウントを持っていない方は<a href={{ route('register') }}>こちら</a>から。</p>
    </section>

    <section>
        <h1>ログイン</h1>
        <p>写真・画像を投稿するためには<a href={{ route('login') }}>ログイン</a>する必要があります。</p>
    </section>
    @endauth

    <section class="posts">
        <h1 class="trend"><a href="{{ route('picture.index') }}">トレンド</a></h1>

        {{ $pictures->onEachSide(2)->links() }}

        @foreach ($pictures as $picture)
        <section class="post">
            <h1>{{ $picture->title }}</h1>
            
            <a href="{{ route('picture.show', ['picture' => $picture->id]) }}"><img src="{{ asset('storage/pictures/'.$picture->path) }}"></a>

            <div class="favorite">
                いいね:{{ $picture->favorite }}
            </div>

            <div class="creater">
                @if($picture->user == null)
                <img><i>退会したユーザ</i>
                @else
                <img src="{{ asset('storage/profiles/'.$picture->user->profile_image) }}">
                <a href="{{ route('user.show', ['user' => $picture->user_id]) }}">{{ $picture->user->name }}</a>
                @endif
            </div>
        </section>
        @endforeach

        {{ $pictures->onEachSide(2)->links() }}

    </section>
@endsection