@extends('layouts.layout')

@section('title', full_title('トレンド'))

@section('content')
  <div class="posts">
    <h1>トレンド</h1>

    {{ $pictures->onEachSide(2)->links() }}

    @foreach ($pictures as $picture)
    <section class="post">
      <h1>{{ $picture->title }}</h1>

      <a href="{{ route('picture.show', ['picture' => $picture->id]) }}"><img src="{{ asset('storage/pictures/'.$picture->path) }}"></a>
      
      <div class="creater">
        @if($picture->user == null)
        <img><i>退会したユーザー</i>
        @else
        <img src="{{ asset('storage/profiles/'.$picture->user->profile_image) }}">
        <a href="{{ route('user.show', ['user' => $picture->user_id]) }}">{{ $picture->user->name }}</a>
        @endif
      </div>
    </section>
    @endforeach

    {{ $pictures->onEachSide(2)->links() }}
    
  </div>
@endsection