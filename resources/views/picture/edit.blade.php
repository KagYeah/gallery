@extends('layouts.layout')

@section('title', full_title('「'.$picture->title.'」の編集'))

@section('content')
<div class="post-picture">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <form method="post" action="{{ route('picture.update', ['picture' => $picture->id]) }}" enctype="multipart/form-data">
          @csrf
          @method('PATCH')

          <input type="hidden" name="user_id" value="{{ $picture->user->id }}">

          <div class="row justify-content-center" id="picture">
            <img src="{{ asset('storage/pictures/'.$picture->path) }}" id="img">
          </div>

          <div class="form-group row">
            <div class="col-md-4 text-md-right">
              <label for="title">タイトル</label>
            </div>

            <div class="col-md-8">
              <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="@if(old('title') == null){{ $picture->title }}@else{{ old('title') }}@endif">

              @error('title')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
          </div>

          <div class="form-group row">
            <div class="col-md-4 text-md-right">
              <label for="title">コメント</label>
            </div>
            
            <div class="col-md-8">
              <textarea class="form-control @error('comment') is-invalid @enderror" name="comment">@if(old('comment') == null){{ $picture->comment }}@else{{ old('comment') }}@endif</textarea>

              @error('comment')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
          </div>

          <div class="form-group row">
            <div class="col-md-6 offset-md-3 text-center">
              <button type="submit" class="btn btn-primary">
                編集
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection