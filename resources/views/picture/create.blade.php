@extends('layouts.layout')

@section('title', full_title('投稿'))

@section('content')
  <div class="post-picture">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <form method="post" action="{{ route('picture.store') }}" enctype="multipart/form-data">
            @csrf

            <input type="hidden" name="user_id" value="{{ $user->id }}">

            <div class="row justify-content-center" id="picture">
            </div>

            <div class="form-group row justify-content-center" style="margin-bottom: 0;">
              <label for="picture-input" class="btn btn-light @error('picture') is-invalid @enderror">
                写真を選択
                <input id="picture-input" type="file" name="picture" onchange="previewImage(this);">
              </label>
            </div>
            
            <div class="row justify-content-center">
              @error('picture')
                <span style="color: #e3342f; font-size: 12px; margin: 0 0 16px;" role="alert">
                  <strong>{{ $errors->first('picture') }}</strong>
                </span>
              @enderror
            </div>

            <div class="form-group row">
              <div class="col-md-4 text-md-right">
                <label for="title">タイトル</label>
              </div>

              <div class="col-md-8">
                <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}">

                @error('title')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <div class="col-md-4 text-md-right">
                <label for="title">コメント</label>
              </div>
              
              <div class="col-md-8">
                <textarea class="form-control @error('comment') is-invalid @enderror" name="comment">{{ old('comment') }}</textarea>

                @error('comment')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <div class="col-md-6 offset-md-3 text-center">
                <button type="submit" class="btn btn-primary">
                  投稿
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script>
    function previewImage(obj)
    {
      if (document.getElementById('img') === null) {
        const img = document.createElement('img');
        img.src = "";
        img.id = "img";
        document.getElementById("picture").appendChild(img);
      }
      var fileReader = new FileReader();
      fileReader.onload = (function() {
        document.getElementById('img').src = fileReader.result;
      });
      fileReader.readAsDataURL(obj.files[0]);
    }
  </script>
@endsection