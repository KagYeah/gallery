@extends('layouts.layout')

@section('title', full_title($title))


@section('content')
  <div class="search container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <h1>画像を検索</h1>
        <form method="get" action="{{ route('picture.search') }}">
          @csrf

          <div class="row m-1 form-group">
            <div class="col-9 p-0">
              <input type="text" name="input" value="{{ $input }}" class="form-control">
            </div>
            <div class="col-3 p-0 text-center">
              <button class="btn btn-primary">検索</button>
            </div>
          </div>
          
          <div class="row m-1 form-group">
            <div class="col-9 p-0">
              <select name="order" class="form-control">
                <option value="新しい順" @if ($order == "新しい順") selected @endif>新しい順</option>
                <option value="古い順" @if ($order == "古い順") selected @endif>古い順</option>
                <option value="いいね順" @if ($order == "いいね順") selected @endif>いいね順</option>
              </select>
            </div>
          </div>
        </form>

        @if ($title != '画像を検索')
        <h1>{{ $title }}</h1>

        <div class="posts">
          {{ $pictures->appends(['input'=>$input, 'order'=>$order])->onEachSide(2)->links() }}

          @foreach ($pictures as $picture)
          <section class="post">
            <h1>{{ $picture->title }}</h1>

            <a href="{{ route('picture.show', ['picture' => $picture->id]) }}"><img src="{{ asset('storage/pictures/'.$picture->path) }}"></a>

            <div class="favorite">
              いいね:{{ $picture->favorite }}
            </div>
            
            <div class="creater">
              @if($picture->user == null)
              <img><i>退会したユーザ</i>
              @else
              <img src="{{ asset('storage/profiles/'.$picture->user->profile_image) }}">
              <a href="{{ route('user.show', ['user' => $picture->user_id]) }}">{{ $picture->user->name }}</a>
              @endif
            </div>
          </section>
          @endforeach

          {{ $pictures->appends(['input'=>$input, 'order'=>$order])->onEachSide(2)->links() }}
        </div>
        @else
        キーワードを入力してください
        @endif
      </div>
    </div>
  </div>
@endsection
