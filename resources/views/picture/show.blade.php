@extends('layouts.layout')

@section('title', full_title($picture->title))

@section('content')
<div class="show-picture">
  <div class="container">
    <div class="row justify-content-center title">
      <h1>{{ $picture->title }}</h1>
    </div>
    <div class="row justify-content-center picture">
      <img src="{{ asset('storage/pictures/'.$picture->path) }}" alt="画像">
    </div>

    @isset($picture->comment)
    <div class="row justify-content-center comment">
      <p><i>{!! nl2br($picture->comment) !!}</i></p>
    </div>
    @endisset

    <div class="row justify-content-start favorite">
      いいね:{{ $picture->favorite }}
    </div>

    <div class="row justify-content-start creater">
      @if($picture->user == null)
      <img><i>退会したユーザ</i>
      @else
      <img src="{{ asset('storage/profiles/'.$picture->user->profile_image) }}">
      <a href="{{ route('user.show', ['user' => $picture->user_id]) }}">{{ $picture->user->name }}</a>
      @endif
    </div>

    @auth
    @if ($favorite == null)
    <div class="row justify-content-center actions">
      <a href="" onclick="document.favorite_form.submit(); return false">いいね</a>

      <form name="favorite_form" type="hidden" method="post" action="{{ route('favorite.store') }}">
        @csrf
        <input type="hidden" name="user_id" value="{{ $user->id }}">
        <input type="hidden" name="picture_id" value="{{ $picture->id }}">
      </form>
    </div>
    @else
    <div class="row justify-content-center actions favorited">
      <a href="" onclick="document.delete_favorite_form.submit(); return false">いいね済</a>

      <form name="delete_favorite_form" type="hidden" method="post" action="{{ route('favorite.destroy', ['favorite' => $favorite->id]) }}">
        @csrf
        @method('DELETE')
      </form>
    </div>
    @endif
    @endauth

    <div class="row justify-content-center download">
      <a href="{{ route('download', ['id' => $picture->id]) }}">ダウンロード</a>
    </div>

    @if ($user != null && $picture->user_id == $user->id)
    <div class="row justify-content-center actions">
      <a href="{{ route('picture.edit', ['picture' => $picture->id]) }}">編集</a>
    </div>
    
    <div class="row justify-content-center actions">
      <a href="" onclick="javascript:if(confirm('本当に削除しますか？')) { document.delete_form.submit(); } return false">削除</a>

      <form type="hidden" name="delete_form" method="post" action="{{ route('picture.destroy', ['picture' => $picture->id]) }}">
        @csrf
        @method('DELETE')
      </form>
    </div>
    @endif
  </div>
</div>
@endsection