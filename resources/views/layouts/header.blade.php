<header>
    <nav class="navbar navbar-expand-sm navbar-light">
        <a href="{{ route('home') }}" class="navbar-brand logo">PicGallery</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#menu">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div id="menu" class="collapse navbar-collapse">
            <ul class="navbar-nav">
                <li><a href="{{ route('search') }}">検索</a></li>
                @auth
                <li class="nav-item"><a href="{{ route('picture.create') }}">投稿する</a></li>
                <li class="nav-item"><a href="{{ route('mypage') }}">マイページ</a></li>
                <li class="nav-item"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">ログアウト</a></li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                @else
                <li class="nav-item"><a href="{{ route('register') }}">新規登録</a></li>
                <li class="nav-item"><a href="{{ route('login') }}">ログイン</a></li>
                @endauth
            </ul>
        </div>
    </nav>
</header>