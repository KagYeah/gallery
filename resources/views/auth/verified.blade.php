@extends('layouts.layout')

@section('title', full_title('メールアドレスの確認'))

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">メールアドレスの確認</div>

                <div class="card-body">
                    <p>メールアドレスが確認されました。</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection