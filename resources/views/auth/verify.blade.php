@extends('layouts.layout')

@section('title', full_title('メールアドレスの確認'))

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">メールアドレスの確認</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            新しい確認用URLを送信しました
                        </div>
                    @endif

                    メールを確認してください
                    もしメールが送信されなかったら、
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">ここをクリックしてください</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
