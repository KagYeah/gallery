@extends('layouts.layout')

@section('title', full_title('error419'))

@section('content')
    <div class="error-page">
        <h1>419 Page Expired</h1>
        <p>CSRF対策が失敗しています。</p>
    </div>
@endsection