@extends('layouts.layout')

@section('title', full_title('error404'))

@section('content')
    <div class="error-page">
        <h1>404 Not Found</h1>
        <p>指定されたページが見つかりません。</p>
    </div>
@endsection
