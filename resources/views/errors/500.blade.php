@extends('layouts.layout')

@section('title', full_title('error500'))

@section('content')
    <div class="error-page">
        <h1>500 Server Error</h1>
        <p>申し訳ありません。</p>
        <p>システムエラーが発生しました。</p>
    </div>
@endsection
