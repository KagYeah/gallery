@extends('layouts.layout')

@section('title', full_title('error401'))

@section('content')
    <div class="error-page">
        <h1>401 Unauthorized</h1>
        <p>認証されていません。</p>
        <p>認証してからアクセスしてください。</p>
    </div>
@endsection
