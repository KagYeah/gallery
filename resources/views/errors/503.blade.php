@extends('layouts.layout')

@section('title', full_title('error503'))

@section('content')
    <div class="error-page">
        <h1>503 Unavailable</h1>
        <p>申し訳ありません。</p>
        <p>只今利用することができません。</p>
        <p>しばらくしてからアクセスしてください。</p>
    </div>
@endsection
