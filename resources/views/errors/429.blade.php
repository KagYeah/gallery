@extends('layouts.layout')

@section('title', full_title('error429'))

@section('content')
    <div class="error-page">
        <h1>429 Too Many Requests</h1>
        <p>大量のリクエストが送信されました。</p>
        <p>もう一度送信してください。</p>
    </div>
@endsection