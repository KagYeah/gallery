@extends('layouts.layout')

@section('title', full_title('error403'))

@section('content')
    <div class="error-page">
        <h1>403 Forbidden</h1>
        <p>指定されたページを閲覧する権限がありません。</p>
    </div>
@endsection
