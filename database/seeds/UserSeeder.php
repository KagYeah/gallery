<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Picture;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 50; $i++) {
            $user = factory(User::class)->create(['name' => 'user'.$i]);
            $pictures = [];
            for($j = $i * 10; $j < $i * 10 + 10; $j++) {
                array_push($pictures, factory(Picture::class)->state('no user_id')->make([
                    'title' => 'title'.$j,
                    'comment' => 'comment'.$j,
                ])->toArray());
            }
            $user->pictures()->createMany($pictures);
        }
    }
}
