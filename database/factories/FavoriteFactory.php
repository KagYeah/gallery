<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\User;
use App\Picture;
use App\Favorite;

$factory->define(Favorite::class, function (Faker $faker) {
    return [
        'user_id' => factory(User::class),
        'picture_id' => factory(Picture::class),
    ];
});

$factory->state(Favorite::class, 'no picture_id', [
    'picture_id' => null,
]);
