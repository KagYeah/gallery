<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\User;
use App\Picture;
use Illuminate\Support\Str;

$factory->define(Picture::class, function (Faker $faker) {
    return [
        'title' => $faker->title(),
        'path' => Str::random(20).'.png',
        'comment' => Str::random(10),
        'user_id' => factory(User::class),
    ];
});

$factory->state(Picture::class, 'no user_id', [
    'user_id' => null,
]);
